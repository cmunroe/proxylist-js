"use strict";
const r = require('axios');
var list;

function update(){
    return new Promise((resolve, reject) => {
        const proxylists = r.get('https://github.com/firehol/blocklist-ipsets/raw/master/proxylists.ipset');
        const proxyrss = r.get('https://github.com/firehol/blocklist-ipsets/raw/master/proxyrss.ipset');
        const maxmind = r.get('https://github.com/firehol/blocklist-ipsets/raw/master/maxmind_proxy_fraud.ipset');
        const nvpn1 = r.get('https://gist.githubusercontent.com/triggex/c6bc554410a84ea1b3ef1c19c5a92d49/raw/1d5a60401f631356d156c21b32471d15dff2e0e1/NordVPN-Server-IP-List-2020.txt');
        const nvpn2 = r.get('https://gist.githubusercontent.com/JamoCA/eedaf4f7cce1cb0aeb5c1039af35f0b7/raw/b038335fdf60ca0764a3b5f8db00ee4fb2792539/NordVPN-Server-IP-List.txt');
        Promise.all([proxylists,proxyrss,maxmind,nvpn1,nvpn2])
        .then(values => {
            var data;
            values.forEach(value => {
                data += value.data;
    
            });
            return data;
        })
        .then(values => {
            return values.split("\n")
        })
        .then(values => {
            list = values;
            resolve();
        })
        .catch(errors => {
            reject(errors);
        })

    })
    

}

function check(ip){
    if(list){
        return list.includes(ip);
    }
    else{
        return null;
    }
}


update();
setInterval(update, 1000 * 60 * 60 * 24);
module.exports = {check, update};